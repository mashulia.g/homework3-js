let firstNumber = +prompt('Write first number');
while (!firstNumber || firstNumber === ""){
    firstNumber = +prompt('Write first number');
}
let secondNumber = +prompt('Write second number');
while (!secondNumber || secondNumber === ""){
    secondNumber = +prompt('Write second number');
}
let mathOperator = prompt('Write an operation: +, -, *, /');

function getCalculations (firstNumber, secondNumber, mathOperator) {
    if (mathOperator === '+'){
        return firstNumber + secondNumber;
    } else if (mathOperator === '-'){
        return firstNumber - secondNumber;
    } else if (mathOperator === '*'){
        return firstNumber * secondNumber;
    } else if (mathOperator === '/'){
        return firstNumber / secondNumber;
    }
}
// console.log(firstNumber + " " + mathOperator + " " + secondNumber);
console.log(getCalculations(firstNumber, secondNumber, mathOperator));
